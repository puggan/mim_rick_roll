(function(d) {
	if(d.getElementById('rickroll'))
	{
		return;
	}
	var b = d.createElement('div');
	b.id = 'rickroll';
	b.style.position = 'fixed';
	b.style.top = '10px';
	b.style.left = '10px';
	b.style.padding = '10px';
	b.style.background = 'black';
	b.style.color = 'yellow';
	b.style.border = 'dashed 5px red';
	b.style.opacity = 1;
	b.style.zIndex = 1;
	b.style.maxWidth = 'calc(100vw - 20px)';
	b.style.maxHeight = 'calc(100vh - 20px)';
	b.innerHTML = '<h1>' +
		'HTTP is insecure' +
		'<span id="norickroll">X</span>' +
		'</h1>' +
		'<p>Please use https:// for your own safty</p>' +
		'<video controls autoplay>' +
		'<source src="https://sundragon.se/rr.mp4" type="video/mp4" />' +
		'</video>';
	var s = document.createElement('style');
	s.type = 'text/css';
	s.appendChild(document.createTextNode('body > * {z-index: -1; opacity: 0.1;}'));
	d.body.append(b);
	d.head.append(s);
	var x = d.getElementById('norickroll');
	x.style.color = 'red';
	x.style.float = 'right';
	x.style.cursor = 'pointer';
	x.addEventListener('click', function(e) {
		b.parentNode.removeChild(b);
		s.parentNode.removeChild(s);
	});
})(document);
