# Rick roll http
# Allow ctest.cdn.nintendo.net 62.127.145.5[67]
iptables -t nat -A PREROUTING -i ${LAN} -p tcp --dport 80 -d 62.127.145.56 -j ACCEPT
iptables -t nat -A PREROUTING -i ${LAN} -p tcp --dport 80 -d 62.127.145.57 -j ACCEPT
# Allow feu01.ps3.update.playstation.net 62.127.145.15[25]
iptables -t nat -A PREROUTING -i ${LAN} -p tcp --dport 80 -d 62.127.145.152 -j ACCEPT
iptables -t nat -A PREROUTING -i ${LAN} -p tcp --dport 80 -d 62.127.145.155 -j ACCEPT
# Redirect all other
iptables -t nat -A PREROUTING -i ${LAN} -p tcp --dport 80 -j DNAT --to-destination 192.168.111.1:114
# Block spam for ppa.launchpad.net 91.189.95.83
iptables -A INPUT -i ${LAN} -p tcp --dport 80 -d 91.189.95.83 -j DROP
iptables -A FORWARD -p tcp -d 192.168.111.1 --dport 114 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
